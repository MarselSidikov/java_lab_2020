package labmda;

import java.util.function.Function;

/**
 * 22.09.2021
 * 25. Lambda, Classloaders and Proxy
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public class Main {
    public static void main(String[] args) {
        // a -> a.method() -> A::method
        // (a,b) -> x.method(a,b)
        // (a1, ..., aN) -> x.method(a1, ..., aN) = x::method
        Function<String, String> stringFunction = (s) -> s.toLowerCase();

//        TwoArgumentProcess process = (a, b) -> StringProcessor.process(a, b);
        TwoArgumentProcess process = StringProcessor::process;

        StringProcessor processor = new StringProcessor("MESSAGE");
//        TwoArgumentProcess process1 = (a, b) -> processor.stringsWithPrefix(a, b);
        TwoArgumentProcess process1 = processor::stringsWithPrefix;

        System.out.println(StringProcessor.process(stringFunction, "ПРИвет"));

    }
}
