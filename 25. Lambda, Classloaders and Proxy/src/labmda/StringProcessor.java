package labmda;

import java.util.function.Function;

/**
 * 22.09.2021
 * 25. Lambda, Classloaders and Proxy
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public class StringProcessor {

    private String prefix;

    public StringProcessor(String prefix) {
        this.prefix = prefix;
    }

    public String stringsWithPrefix(String x, String y) {
        return prefix + " " + x + " " + y;
    }

    public static String process(Function<String, String> function, String text) {
        return function.apply(text);
    }

    public static String process(String x, String y) {
        return x.toLowerCase() + " " + y.toLowerCase();
    }
}
