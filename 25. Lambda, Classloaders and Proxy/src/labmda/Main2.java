package labmda;

import java.util.Comparator;
import java.util.function.ToIntFunction;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * 22.09.2021
 * 25. Lambda, Classloaders and Proxy
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public class Main2 {
    public static void main(String[] args) {
        // (o1, o2) -> Integer.compare(o1.length(), o2.length())
        // Comparator.comparingInt(String::length)
        // String::length -> s -> s.length()
        //
        Stream.of("Привет", "Как дела?", "Что")
                .sorted(Comparator.comparingInt(String::length))
                .collect(Collectors.toList());
                                                    // s -> s.length()
        ToIntFunction<String> stringToIntFunction = value -> 0;
    }
}
