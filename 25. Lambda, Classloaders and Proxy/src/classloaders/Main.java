package classloaders;

/**
 * 22.09.2021
 * 25. Lambda, Classloaders and Proxy
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public class Main {
    public static void main(String[] args) throws Exception {
        FolderClassLoader folderClassLoader = new FolderClassLoader("C:\\Users\\Marsel\\Desktop\\Classes");
        System.out.println(User.class.getClassLoader());
        System.out.println(String.class.getClassLoader());
        Class<?> exampleClass = Class.forName("Example", true, folderClassLoader);
        System.out.println(exampleClass.getClassLoader());
        System.out.println(exampleClass.getDeclaredMethods().length);
    }
}
