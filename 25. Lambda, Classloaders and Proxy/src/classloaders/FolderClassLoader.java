package classloaders;

import java.io.FileInputStream;
import java.io.InputStream;

/**
 * 22.09.2021
 * 25. Lambda, Classloaders and Proxy
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public class FolderClassLoader extends ClassLoader {
    private String folderName;

    public FolderClassLoader(String folderName) {
        this.folderName = folderName;
    }

    @Override
    protected Class<?> findClass(String name) throws ClassNotFoundException {
        try (InputStream input = new FileInputStream(folderName + "/" + name + ".class")) {
            byte[] bytes = new byte[input.available()];
            int bytesCount = input.read(bytes);
            return defineClass(name, bytes, 0, bytesCount);
        } catch (Exception e) {
            throw new IllegalArgumentException(e);
        }
    }
}
