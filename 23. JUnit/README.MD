# Модульное тестирование

* Автоматические проверки публичных методов классов на основе известных входных и выходных данных.

* JUnit 5 - фреймворк модульного тестирования.

Необходимо:

- Соблюдать best practices модульного тестирования
- Использовать аннотации JUnit
- Использовать hamcrest для повышения читаемости
- Группировка тестов во вложенные классы
- Именование методов для повышения читаемости
- Тестируем каждый модуль отдельно, зависимости делаем Mock-объектами

* [Junit](https://junit.org/junit5/docs/current/user-guide/)

* [Hamcrest API](http://hamcrest.org/JavaHamcrest/javadoc/2.2/org/hamcrest/Matchers.html)

* [JUnit Best Practices](https://phauer.com/2019/modern-best-practices-testing-java/)

* [Hamcrest Example](https://sysout.ru/rabota-s-hamcrest/)