package ru.itis.math;

/**
 * 11.05.2021
 * junit-example
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public interface NumbersToBooleanMapper {
    boolean map(int value);
}
