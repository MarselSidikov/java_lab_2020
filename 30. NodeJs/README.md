# Docker

* Система контейнеризации, позволяет изолировать различные области друг от друга.
* Хорошо подходит для передачи проектов как блоков инфраструктур (приложение + база данных + очередь сразу).

* Образ (Image) - то, на основе чего создается контейнер 
* Контейнер - изолированная среда для запуска приложения
* Чтобы создать контейнер, необходимо собрать образ.
* DockerHub - репозиторий готовых образов
* Каждый образ включает в себя "маленький дистрибутив Linux"
* Образы можно наследовать друг от друга

* Создание volume - `docker volume create node-app-volume`
* Запуск контейнера NodeJs - `docker run --name node-app-container-1 -v node-app-volume:/usr/applications/nodejs-app/storage -p 80:8080 -d nodejs-app-image`
* Запуск контейнера PostgreSQL - `docker run --name postgresql-container -p 5433:5432 -e POSTGRES_PASSWORD=qwerty009 -e POSGTRES_DB=app_db -d -v pgdata:/var/lib/postgresql/data postgres` 

* Расположение volume на Windows

```
\\wsl$\docker-desktop-data\version-pack-data\community\docker\volumes\
```

## TASK

* Запустить RabbitMQ в докере и ваше приложение должно к нему подключаться.
* Запустить Postgres в докере и ваше приложение должно к нему подключаться.