package ru.itis.javalab.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.itis.javalab.models.Product;

/**
 * 22.02.2021
 * 20. Web MVC Spring Data Jpa
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public interface ProductsRepository extends JpaRepository<Product, Long> {
}
