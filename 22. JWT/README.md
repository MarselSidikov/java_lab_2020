# JWT

* JWT - JSON закодированный в формате Base64
```
eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9 - HEADER (алгоритм шифрования подписи + служебная информация)
.
eyJzdWIiOiIxIiwiZW1haWwiOiJzaWRpa292Lm1hcnNlbEBnbWFpbC5jb20iLCJyb2xlIjoiQURNSU4iLCJzdGF0ZSI6IkFDVElWRSJ9 - PAYLOAD (полезная нагрузка)
.
3AwQqJF5XiYlqEVQ2Sjx2I7puiTI0vYWYupLev6Lf2A - VERIFY SIGNATURE (подпись токена)
```

* Какие проблемы с токеном?

1. Мы храним некоторые данные в открытом виде - не надо хранить действительно важные данные (пароли).
2. Его могут похитить - тут ничего не поделаешь.
3. Могут изменить его содержимое, и например, вместо `role ADMIN` указать `role SUPERADMIN` и делать страшные вещи из под суперадмина

* У нас есть решение 3-ей проблемы - мы всегда можем доверять содержимому токена.

```
Подпись

HMACSHA256(
  base64UrlEncode(header) + "." +
  base64UrlEncode(payload),
  seckret_key

```

Поскольку подпись без ключа нельзя вскрыть, подпись формируется на сервере, и формируется она исходя из содержимого payload
если кто-то изменит его содержимое - то токен не пройдет проверку подписи
