package ru.itis.app;

import com.auth0.jwt.JWT;
import com.auth0.jwt.algorithms.Algorithm;
import ru.itis.models.User;

/**
 * 13.04.2021
 * JWT
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public class MainCreateToken {
    public static void main(String[] args) {
        User user = User.builder()
                .id(10L)
                .firstName("Александр")
                .lastName("Ференец")
                .email("aferenetz@kpfu.ru")
                .role(User.Role.ADMIN)
                .state(User.State.ACTIVE)
                .build();

        String token = JWT.create()
                .withSubject(user.getId().toString())
                .withClaim("role", user.getRole().toString())
                .withClaim("state", user.getState().toString())
                .withClaim("email", user.getEmail())
                .sign(Algorithm.HMAC256("seckret_key"));

        System.out.println(token);

    }
}
