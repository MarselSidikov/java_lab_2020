```
Hash Join  (cost=79.86..252.65 rows=118 width=8) -- строит хеш-таблицу и сравнивает с ней другую таблицу
  Hash Cond: (l.course_id = c.id) 
  ->  Seq Scan on lesson l  (cost=0.00..169.85 rows=1119 width=4) -- вытаскиваем все уроки, которые есть в понедельник
        Filter: ((day)::text = 'MONDAY'::text)
  ->  Hash  (cost=70.86..70.86 rows=720 width=8)
        ->  Bitmap Heap Scan on course c  (cost=17.86..70.86 rows=720 width=8) -- получаем все курсы, у которых цена больше > 100
              Recheck Cond: (price > 100)
              ->  Bitmap Index Scan on course_index  (cost=0.00..17.68 rows=720 width=0) > используется индекс
                    Index Cond: (price > 100)

```

```
HashCond: выражения для сравнения
    -> первая выборка
    -> Hash
        -> вторая выборка
```

Первая выборка - все уроки, которые по понедельникам
id урока, id-курса
432     | 15
Вторая выборка - хеш-таблица
id-курса|
12      |price = 499
15      |prive = 102