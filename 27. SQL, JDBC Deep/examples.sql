-- CTE
-- в какие даты рождались студенты, которые ходят на курсы по понедельникам?
with id_of_course_with_mondays as (
    select distinct course_id
    from lesson
    where lesson.day = 'MONDAY'
),
     id_of_student_in_course_with_mondays as (
         select student_id
         from student_course sc
                  inner join id_of_course_with_mondays cwn on sc.course_id = cwn.course_id
     ),
     student_with_valid_birthdate as (
         select id, birth_date from student where birth_date between '2000-01-01' and '2021-12-31'
     )
select distinct (birth_date)
from student_with_valid_birthdate s1
         inner join id_of_student_in_course_with_mondays s2 on s1.id = s2.student_id
order by birth_date;

create index course_index on course (price);

explain analyse
select *
from course
where price > 50; -- 0.9 - 2.1 мс, после индекса - 0.2 - 0.9

set enable_seqscan = on;
explain analyse
select *
from student
where birth_date > '2000-01-01'; -- 2 до 6 мс

create index birthdate_idx on student ((birth_date::DATE));

SELECT tablename,
       indexname,
       indexdef
FROM pg_indexes
WHERE schemaname = 'public'
ORDER BY tablename,
         indexname;

-- получить стоимость и id курсов у которых есть уроки по вторникам и их стоимость больше 100
explain
select c.id, c.price
from course c
         inner join (select l.course_id from lesson l where l.day = 'MONDAY') mondays on c.id = mondays.course_id
where c.price > 100;

select * from course limit 10;