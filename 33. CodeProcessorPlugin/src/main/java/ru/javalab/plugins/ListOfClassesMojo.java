package ru.javalab.plugins;

import org.apache.maven.plugin.AbstractMojo;
import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugin.MojoFailureException;
import org.apache.maven.plugins.annotations.LifecyclePhase;
import org.apache.maven.plugins.annotations.Mojo;
import org.apache.maven.plugins.annotations.Parameter;
import org.apache.maven.project.MavenProject;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

/**
 * 19.05.2022
 * 33. CodeProcessorPlugin
 *
 * @author Sidikov Marsel (Akvelon)
 * @version v1.0
 */
@Mojo(name = "list-of-classes", defaultPhase = LifecyclePhase.PROCESS_SOURCES)
public class ListOfClassesMojo extends AbstractMojo {

    @Parameter(defaultValue = "${project}", required = true, readonly = true)
    private MavenProject project;

    @Parameter(defaultValue = "${project.build.outputDirectory}", required = true)
    private String outputDirectoryFileName;

    @Parameter(name = "listOfClassesFileName", required = true)
    private String listOfClassesFileName;

    @Override
    public void execute() throws MojoExecutionException, MojoFailureException {
        File outputDirectory = new File(outputDirectoryFileName);

        File listOfClassesFile = new File(outputDirectory, listOfClassesFileName);

        try (BufferedWriter writer = new BufferedWriter(new FileWriter(listOfClassesFile))) {
            getLog().info("Output file for list of classes is <" + listOfClassesFileName + ">");
            String sourceDirectoryFileName = project.getBuild().getSourceDirectory();

            Files.walk(Paths.get(sourceDirectoryFileName))
                    .filter(Files::isRegularFile)
                    .forEach(file -> {
                        try {
                            writer.write(file.getFileName().toString());
                            writer.newLine();
                        } catch (IOException e) {
                            throw new IllegalArgumentException(e);
                        }
                    });

            getLog().info("Finish work");
        } catch (IOException e) {
            throw new MojoExecutionException(e);
        }
    }
}
