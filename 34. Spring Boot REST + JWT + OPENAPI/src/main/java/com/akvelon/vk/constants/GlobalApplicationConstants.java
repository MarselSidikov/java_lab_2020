package com.akvelon.vk.constants;

/**
 * 22.06.2022
 * 29. Spring Boot REST + Security
 *
 * @author Sidikov Marsel (Akvelon)
 * @version v1.0
 */
public class GlobalApplicationConstants {
    public static final String AUTHENTICATION_URL = "/auth/token";

}
