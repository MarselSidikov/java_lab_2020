package com.akvelon.vk.aspects;

import com.akvelon.vk.dto.WrappedResponse;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.After;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;

/**
 * 14.06.2022
 * 26. Spring Boot REST
 *
 * @author Sidikov Marsel (Akvelon)
 * @version v1.0
 */
@Component
@Aspect
public class TimeAspect {
    @Around(value = "execution(* com.akvelon.vk.controllers.LessonsController.*(..))")
    public ResponseEntity<WrappedResponse> addTimeToResponse(ProceedingJoinPoint joinPoint) throws Throwable {
        long before = System.currentTimeMillis();
        Object response = joinPoint.proceed();
        long after = System.currentTimeMillis();
        long time = after - before;

        return ResponseEntity.ok(WrappedResponse.builder()
                .response(((ResponseEntity) response).getBody())
                .time(time)
                .build());
    }
}
