package com.akvelon.vk.security.utils;

import org.springframework.security.core.Authentication;

import java.util.Map;

/**
 * 22.06.2022
 * 29. Spring Boot REST + Security
 *
 * @author Sidikov Marsel (Akvelon)
 * @version v1.0
 */
public interface JwtUtil {
    Map<String, String> generateTokens(String subject, String authority, String issuer);

    Authentication buildAuthentication(String token);
}
