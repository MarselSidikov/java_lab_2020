package com.akvelon.vk.dto;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

/**
 * 01.07.2022
 * 29. Spring Boot REST + Security
 *
 * @author Sidikov Marsel (Akvelon)
 * @version v1.0
 */
@Schema(description = "Форма для ввода логина и пароля")
@Data
public class EmailAndPassword {
    private String email;
    private String password;
}
