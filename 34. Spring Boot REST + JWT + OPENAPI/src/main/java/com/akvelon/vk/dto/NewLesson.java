package com.akvelon.vk.dto;

import com.akvelon.vk.validation.annotations.NotSameNames;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;

/**
 * 09.06.2022
 * 26. Spring Boot REST
 *
 * @author Sidikov Marsel (Akvelon)
 * @version v1.0
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Schema(description = "Урок")
@NotSameNames(fields = {"name", "description"}, message = "{newLesson.names.same}")
public class NewLesson {
    @Schema(description = "Название урока", minLength = 2, example = "Урок")
    @Size(min = 2, max = 15, message = "{newLesson.name.size}")
    private String name;

    @NotEmpty(message = "{newLesson.description.empty}")
    private String description;
}
