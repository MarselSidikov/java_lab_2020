# Open API
    
* [OpenAPI Руководство](https://starkovden.github.io/step5-components-object.html)
* [Спецификация](https://github.com/OAI/OpenAPI-Specification/blob/main/versions/3.0.2.md)
* [Документация по приложению](http://localhost/v3/api-docs)
* [JSON Validator для удобства](https://jsonformatter.curiousconcept.com/#)

# JWT

* Некоторая строка, которую может проверить сервер, основываясь только на этой строке.
* Данная строка подписывается сервером, и поэтому он может ей доверять.
* Главное преимущество - не нужно идти в базу данных при каждом запросе

### Структура токена

* `HEADER` - содержит служебную информацию о том, каким алгоритмом был подписан данный токен
* `PAYLOAD` - некоторая полезная нагрузка (может содержать email-пользователя, его роль, его state и т.д., кроме пароля)
* `VERIFY SIGNATURE` - подпись данного токена, основанная на алгоритме, указанном в `HEADER`

```
eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9 - HEADER
eyJzdWIiOiJzaWRpa292Lm1hcnNlbEBnbWFpbC5jb20iLCJyb2xlIjoiVVNFUiIsImlzcyI6Imh0dHA6Ly9sb2NhbGhvc3QvYXBpL2xvZ2luIiwiZXhwIjoxNjU1ODkxODMwfQ - PAYLOAD 
VyF2stzcek8lkz3f1W1oDDzd8I9ssbVtNwJ4RlTg7vQ - VERIFY SIGNATURE
```

* Алгоритм формирования токена

```
header = base64("{
  "typ": "JWT",
  "alg": "HS256"
}") -> eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9

payload = base64("{
  "sub": "sidikov.marsel@gmail.com",
  "role": "ADMIN",
  "iss": "http://localhost/api/login",
  "exp": 1655891830
}") -> eyJzdWIiOiJzaWRpa292Lm1hcnNlbEBnbWFpbC5jb20iLCJyb2xlIjoiVVNFUiIsImlzcyI6Imh0dHA6Ly9sb2NhbGhvc3QvYXBpL2xvZ2luIiwiZXhwIjoxNjU1ODkxODMwfQ

verfiy signature = HMACSHA256(base64(header) + "." + base64(payload), secret) -> VyF2stzcek8lkz3f1W1oDDzd8I9ssbVtNwJ4RlTg7vQ

JWT = header.payload.verify_signature
```

### Общий принцип работы токена

* Клиент посылает запрос на сервер с логином и паролем
* Сервер создает JWT-токен, подписывает его ключом, известным ТОЛЬКО серверу
* Отдает клиенту
* Каждый раз, когда клиент посылает запрос, сервер верифицирует токен и разрешает доступ на основе `PAYLOAD`-токена

### Дальше

* Что делать с просрочкой токена?
* Как настроить Swagger?
* Как отзывать токены?
