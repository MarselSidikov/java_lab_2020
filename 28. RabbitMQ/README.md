* Все данные уходят с ключами:

```
image.png -> files.images.png
image.jpg -> files.images.jpg
index.html -> files.documents.html
file.pdf -> files.documents.pdf
```

* `DocumentsDownloader` принимает сообщения внутри своей очереди по шаблону `files.documents.*`
* `ImagesJpegDownloader/ImagesPngDownloader` принимают по шаблонам `files.images.jpg/files.images.png`
* `FilesAnalytics принимаетпо шаблонам` - `files.#"`