package ru.itis.repositories;

import org.springframework.data.keyvalue.repository.KeyValueRepository;
import ru.itis.model.Product;

/**
 * 20.12.2021
 * 51. Redis
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public interface ProductsRedisRepository extends KeyValueRepository<Product, String> {
}
